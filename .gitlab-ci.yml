stages:
  - lint
  - build
  - deploy

image: quay.io/elevorin/manifests-tools:latest

before_script:
  - cd ./deployment

start-pipelinerun:
  stage: build
  script:
    - ansible-playbook -v start-pipelinerun.yml
      -e "env=stage
      deployment_revision=${CI_COMMIT_SHA}
      git_repo_url=${deployment_repo_url}
      git_revision=${CI_COMMIT_SHA}
      git_merge_id=${CI_MERGE_REQUEST_IID}
      git_source_project_id=${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}
      git_target_project_id=${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}
      git_target_repo_url=${deployment_repo_url}
      stream=${stream}
      pipelinerun_name=pipelines-as-code-${CI_MERGE_REQUEST_IID}-${RANDOM}"
  environment:
    name: stage
  only:
    - merge_requests

deploy_stage_product_build:
  stage: deploy
  script:
    - ansible-playbook publish-product-build.yml -e env=stage
  environment:
    name: stage
  when: manual

deploy_prod_product_build:
  stage: deploy
  script:
    - ansible-playbook publish-product-build.yml -e env=prod
  environment:
    name: prod
  when: manual

deploy_stage:
  stage: deploy
  script:
    - ansible-playbook main.yml -e env=stage
  environment:
    name: stage
  when: manual

deploy_prod:
  stage: deploy
  script:
    - ansible-playbook main.yml -e env=prod
  environment:
    name: prod
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

tmt-check:
  image: quay.io/testing-farm/tmt:latest
  stage: lint
  before_script:
    - tmt --version
  script:
    - tmt lint .
    - tmt plans ls
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "ci/**/*.fmf"
  allow_failure: true

shell-check:
  image: koalaman/shellcheck-alpine:stable
  stage: lint
  before_script:
    - shellcheck --version
  script: # TODO should also check for warnings before merge to main
    - find -name '*.sh' -exec shellcheck --severity=error -f gcc {} +
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "**/*.sh"
  allow_failure: true

ansible-check:
  image: quay.io/ansible/toolset:latest
  stage: lint
  before_script:
    - ansible-lint --version
  script:
    - ansible-lint -x var-naming deployment
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "deployment/**/*.yml"
  allow_failure: true

python-check:
  image: registry.access.redhat.com/ubi8/python-39:1-15
  stage: lint
  before_script:
    - pip install --upgrade pip
    - pip install pylint
    - pylint --version
  script:
    # install modules used by the scripts to avoid mis-reporting
    - pip install -r ./deployment/requirements.txt
    - pip install -r ./ci/create_yum_repo/requirements.txt
    - find -name '*.py' -exec pylint {} +
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "**/*.py"
  allow_failure: true
