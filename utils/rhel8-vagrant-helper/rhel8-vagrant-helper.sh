#!/bin/bash

# This helper script is to workaround the fact that vagrant-libvirt is broken
# on Red Hat Enterprise Linux 8 by default. You can see the upstream discussions
# on this topic here [1] and here [2].
#
# This script works around the issues by compiling libssh and krb5 additionally
# for vagrant-libvirt (stored into an embedded libs dir solely for vagrant
# usage).
#
# [1] https://github.com/hashicorp/vagrant/issues/11020
# [2] https://github.com/vagrant-libvirt/vagrant-libvirt/issues/1031

set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Warn user running this script about the actions it undertakes
cat << EOF
Warning: This script will install software and enable dnf software repos on your
machine to enable vagrant + libvirt useage. This is only intended to be run on
RHEL8 machines where vagrant-libvirt is not working out of the box.

 * Install and enable hashicorp dnf repo
 * Install vagrant
 * Install libvirt and enabled libvirtd
 * Install development tools
 * Compile libssh and krb5-libs for vagrant
 * Install vagrant-libvirt plugin

This script will exit on error if any steps fail during the process.
EOF

# Check we're on a Red Hat Enterprise Linux machine
source /etc/os-release
if [ "${NAME}" != "Red Hat Enterprise Linux" ]; then
    echo >&2 "Error: This script expects to only run on Red Hat Enterprise Linux"
    exit 1
fi
if [ "${PLATFORM_ID}" != "platform:el8" ]; then
    echo >&2 "Error: This script expects to only run on Red Hat Enterprise Linux 8"
    exit 1
fi

read -p "Proceed? [y/N] " -n 1 -r
echo    # (optional) move to a new line
# Use non-negated form for safest handling of input
if [[ $REPLY =~ ^[Yy]$ ]]; then
  # Check we can download required source rpms
  # Otherwise throw workaround error message
  if [ ! -e "${DIR}"/libssh-*.src.rpm ]; then
    dnf download --source libssh
    if [ $? -ne 0 ]; then
      cat << EOF >&2
Cannot automatically download libssh src rpm.
Please download the source package from here and place in same directory as this script:
https://access.redhat.com/downloads/content/rhel---8/x86_64/7416/libssh/0.9.4-3.el8/x86_64/fd431d51/package
EOF
      exit 1
    fi
  fi
  if [ ! -e "${DIR}"/krb5-*.src.rpm ]; then
    dnf download --source krb5-libs
    if [ $? -ne 0 ]; then
      cat << EOF >&2
Cannot automatically download krb5-libs src rpm.
Please download the source package from here and place in same directory as this script:
https://access.redhat.com/downloads/content/rhel---8/x86_64/7416/krb5-libs/1.18.2-14.el8/x86_64/fd431d51/package
EOF
      exit 1
    fi
  fi

  # Install vagrant
  echo "Installing vagrant"
  sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
  sudo dnf -y install vagrant

  # Install libvirt
  echo "Installing libvirt"
  cat /proc/cpuinfo | egrep "vmx|svm"
  sudo dnf -y install @virt libvirt-devel
  lsmod | grep kvm
  sudo systemctl enable --now libvirtd

  # Install vagrant-libvirt plugin
  echo "Installing vagrant-libvirt plugin"
  sudo dnf -y groupinstall "Development Tools"
  sudo dnf -y install cmake
  CONFIGURE_ARGS="with-libvirt-include=/usr/include/libvirt with-libvirt-lib=/usr/lib64" \
    vagrant plugin install vagrant-libvirt

  mkdir -p /tmp/krb5-libs
  cd /tmp/krb5-libs
  cp "${DIR}"/krb5-*.src.rpm .
  rpm2cpio krb5-*.src.rpm | cpio -imdV
  tar xf krb5-*.tar.gz
  KRB5_DIR=$(find . -type d -name "krb5-*")
  cd "${KRB5_DIR}/src"
  ./configure
  make
  sudo cp -P lib/crypto/libk5crypto.* /opt/vagrant/embedded/lib64/

  mkdir -p /tmp/libssh
  cd /tmp/libssh
  cp "${DIR}"/libssh-*.src.rpm .
  rpm2cpio libssh-*.src.rpm | cpio -imdV
  tar xf libssh*.tar.xz
  LIBSSH_DIR=$(find . -type d -name "libssh-*")
  mkdir -p build
  cd build
  cmake "../${LIBSSH_DIR}" -DOPENSSL_ROOT_DIR=/opt/vagrant/embedded/
  make
  sudo cp lib/libssh* /opt/vagrant/embedded/lib64

  vagrant plugin install vagrant-libvirt

  echo "Done!"
fi
