# About

The script make-lockfiles.py can be used to re-create the
cs9-image-manifest.lock.json file in the automotive-sig repo.

# Running

You can build and run the container image with the following commands.

```
podman build -t make-lockfile:latest .
podman run make-lockfile:latest
```

If successful, the script will output a lockfile to stdout.

The inputs/outputs are explained in the below sections.

# Inputs

By default the script uses the cs9 results of tiny distro builder
(https://tiny.distro.builders/) as input. The script by default will download
the cs9 environments which contains package definitions for both aarch64 and
x86_64.

Tiny distro builder is fed by four workloads defined here by the BE-team:

(1) https://tiny.distro.builders/workload-overview--automotive-c9s-workload-development--automotive-c9s-repositories.html

(2) https://tiny.distro.builders/workload-overview--automotive-c9s-workload-minimum--automotive-c9s-repositories.html

(3) https://tiny.distro.builders/workload-overview--automotive-c9s-workload-ostree--automotive-c9s-repositories.html

(4) https://tiny.distro.builders/workload-overview--automotive-c9s-workload-podman--automotive-c9s-repositories.html

These are consumed nightly by tiny distro builder and run through
content-resolver (https://github.com/minimization/content-resolver) to generate
the results hosted on the internet.

This results in eight environment files (four for aarch64, four for x86_64).

(1) https://tiny.distro.builders/workload--automotive-c9s-workload-development--automotive-c9s-env-minimum--automotive-c9s-repositories--aarch64.json

(2) https://tiny.distro.builders/workload--automotive-c9s-workload-minimum--automotive-c9s-env-minimum--automotive-c9s-repositories--aarch64.json

(3) https://tiny.distro.builders/workload--automotive-c9s-workload-ostree--automotive-c9s-env-minimum--automotive-c9s-repositories--aarch64.json

(4) https://tiny.distro.builders/workload--automotive-c9s-workload-podman--automotive-c9s-env-minimum--automotive-c9s-repositories--aarch64.json

(5) https://tiny.distro.builders/workload--automotive-c9s-workload-development--automotive-c9s-env-minimum--automotive-c9s-repositories--x86_64.json

(6) https://tiny.distro.builders/workload--automotive-c9s-workload-minimum--automotive-c9s-env-minimum--automotive-c9s-repositories--x86_64.json

(7) https://tiny.distro.builders/workload--automotive-c9s-workload-ostree--automotive-c9s-env-minimum--automotive-c9s-repositories--x86_64.json

(8) https://tiny.distro.builders/workload--automotive-c9s-workload-podman--automotive-c9s-env-minimum--automotive-c9s-repositories--x86_64.json

The defaults can be overriden and the script can be fed parameters that redirect it to load the input json files from local files or urls.

```
--env [local file or url]
```

Simply add as many files or urls as needed after --env.

```
--env [url_1] [url_2]
```

The ability to reference local files is useful when running content-resolver
locally and testing directly the results.

Example:
```
wget 'https://tiny.distro.builders/workload--automotive-c9s-workload-podman--automotive-c9s-env-minimum--automotive-c9s-repositories--aarch64.json' -O ./aarch64.json
wget 'https://tiny.distro.builders/workload--automotive-c9s-workload-podman--automotive-c9s-env-minimum--automotive-c9s-repositories--x86_64.json' -O ./x86_64.json
podman run \
  -v $(pwd):/mnt \
  make-lockfile \
  --env /mnt/aarch64.json /mnt/x86_64.json
```

# Outputs

The script outputs directly to stdout the json file resulting from the two input files.

This outputted file can be used as the cs9-image-manifest.json.

The parameter --outfile can be used to redirect the output to a file instead.

```
podman run \
  -v $(pwd):/mnt \
  make-lockfile \
  --outfile "/mnt/cs9-image-manifest.lock.json"
```

# Debugging

If the script is run with --debug, the script will output debug messages to stderr.

