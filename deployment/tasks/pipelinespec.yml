---
pipelinespec:
  workspaces:
    - name: shared-workspace
  params:
    - name: deployment-repo-url
      type: string
      description: url of the git repo for the code of deployment
      default: "{{ deployment_repo_url }}"
    - name: deployment-revision
      type: string
      description: revision to be used from deployment repo
      default: "{{ deployment_revision }}"
    - name: git-repo-url
      type: string
      description: webhook git repo url
      default: "{{ git_repo_url }}"
    - name: git-revision
      type: string
      description: webhook git repo revision
      default: "{{ git_revision }}"
    - name: merge-request-id
      type: string
      description: webhook git repo merge id
      default: "{{ git_merge_id }}"
    - name: source-project-id
      type: string
      description: webhook git source project id
      default: "{{ git_source_project_id }}"
    - name: target-git-repo-url
      type: string
      description: webhook git target git repo url
      default: "{{ git_target_repo_url }}"
    - name: target-project-id
      type: string
      description: webhook git target project id
      default: "{{ git_target_project_id }}"
    - name: TMT_PLAN
      type: string
      description: the name of the tmt plan to run at Testing Farm
      default: /ci/create-commit
    - name: ARCH
      type: string
      description: target architecture for building imges or running tests
      default: aarch64
    - name: STREAM
      type: string
      description: OS stream to be used upstream (CentOS Stream) or downstream (RHEL)
      default: "{{ stream }}"

  tasks:
    - name: verify-pipelinerun
      params:
        - name: DESCRIPTION
          value: "pipeline status"
        - name: SHA
          value: $(params.git-revision)
        - name: MERGE_REQUEST_IID
          value: $(params.merge-request-id)
        - name: SOURCE_PROJECT_ID
          value: $(params.source-project-id)
        - name: TARGET_PROJECT_ID
          value: $(params.target-project-id)
        - name: STATE
          value: "running"
        - name: NAME
          value: pipeline-status
      taskSpec: "{{ task_gitlab_verify_pipelinerun }}"

    - name: deployment-fetch-repository
      runAfter:
        - verify-pipelinerun
      params:
        - name: url
          value: $(params.deployment-repo-url)
        - name: subdirectory
          value: "toolchain"
        - name: deleteExisting
          value: "true"
        - name: revision
          value: $(params.deployment-revision)
        - name: depth
          value: "0"
      workspaces:
        - name: output
          workspace: shared-workspace
      taskRef:
        kind: ClusterTask
        name: git-clone

    - name: sig-fetch-repository
      runAfter:
        - verify-pipelinerun
      params:
        - name: url
          value: $(params.git-repo-url)
        - name: subdirectory
          value: "manifest_repo"
        - name: deleteExisting
          value: "true"
        - name: revision
          value: $(params.git-revision)
        - name: depth
          value: "0"
      workspaces:
        - name: output
          workspace: shared-workspace
      taskRef:
        kind: ClusterTask
        name: git-clone

    - name: pipeline-guard
      runAfter:
        - sig-fetch-repository
        - deployment-fetch-repository
      params:
        - name: MANIFESTS_UPSTREAM
          value: "$(params.target-git-repo-url)"
        - name: MANIFESTS_MAIN_BRANCH
          value: "main"
        - name: PIPELINES_MAIN_BRANCH
          value: "main"
        - name: PIPELINES_UPSTREAM
          value: "$(params.deployment-repo-url)"
        - name: MANIFESTS_GIT_REPO
          value: $(params.git-repo-url)
        - name: MANIFESTS_GIT_REVISION
          value: $(params.git-revision)
        - name: PIPELINES_GIT_REPO
          value: $(params.deployment-repo-url)
        - name: PIPELINES_GIT_REVISION
          value: $(params.deployment-revision)
      workspaces:
        - name: source
          workspace: shared-workspace
      taskSpec: "{{ task_pipeline_guard }}"

    - name: create-yum-repo
      runAfter:
        - pipeline-guard
      params:
        - name: PIPELINE_RUN_ID
          value: $(context.pipelineRun.uid)
        - name: STREAM
          value: $(params.STREAM)
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_create_yum_repo }}"

    - name: fusa-quality-gate
      runAfter:
        - pipeline-guard
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_fusa_quality_gate }}"

    - name: create-osbuild-cs9-aarch64
      runAfter:
        - create-yum-repo
      params:
        - name: repo_url
          value: $(params.git-repo-url)
        - name: revision
          value: $(params.git-revision)
        - name: REF
          value: $(params.deployment-revision)
        - name: TMT_PLAN
          value: /ci/create-osbuild
        - name: ARCH
          value: aarch64
        - name: OS_VERSION
          value: "9"
        - name: STREAM
          value: $(params.STREAM)
        - name: PIPELINE_RUN_ID
          value: $(context.pipelineRun.uid)
        - name: PIPELINE_TASK_NAME
          value: create-osbuild-cs9-aarch64
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: run-smoke-tests-aarch64
      runAfter:
        - create-osbuild-cs9-aarch64
      params:
        - name: repo_url
          value: $(params.git-repo-url)
        - name: revision
          value: $(params.git-revision)
        - name: REF
          value: $(params.deployment-revision)
        - name: TMT_PLAN
          value: /smoke/plans/MinimalOstree
        - name: ARCH
          value: aarch64
        - name: OS_VERSION
          value: "9"
        - name: STREAM
          value: $(params.STREAM)
        - name: PIPELINE_RUN_ID
          value: $(context.pipelineRun.uid)
        - name: PIPELINE_TASK_NAME
          value: run-smoke-tests-aarch64
        - name: ACTION
          value: TEST
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-cs9-x86-64
      runAfter:
        - create-yum-repo
      params:
        - name: repo_url
          value: $(params.git-repo-url)
        - name: revision
          value: $(params.git-revision)
        - name: REF
          value: $(params.deployment-revision)
        - name: TMT_PLAN
          value: /ci/create-osbuild
        - name: ARCH
          value: x86_64
        - name: OS_VERSION
          value: "9"
        - name: STREAM
          value: $(params.STREAM)
        - name: PIPELINE_RUN_ID
          value: $(context.pipelineRun.uid)
        - name: PIPELINE_TASK_NAME
          value: create-osbuild-cs9-x86-64
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

  finally:
    - name: set-pipeline-note
      params:
        - name: MERGE_REQUEST_IID
          value: $(params.merge-request-id)
        - name: PROJECT_ID
          value: $(params.target-project-id)
        - name: PIPELINE_RUN_ID
          value: $(context.pipelineRun.uid)
        - name: PIPELINE_RUN_NAME
          value: $(context.pipelineRun.name)
        - name: REVISION
          value: $(params.git-revision)
        - name: TASKS
          value:
            - create-yum-repo
            - fusa-quality-gate
            - create-osbuild-cs9-aarch64
            - create-osbuild-cs9-x86-64
            - run-smoke-tests-aarch64
        - name: BUILD_SKIPPED
          value: "$(tasks.pipeline-guard.results.skip-image-build)"
      workspaces:
        - name: source
          workspace: shared-workspace
      taskSpec: "{{ task_gitlab_set_pipeline_note }}"

    - name: create-yum-repo-set-status
      params:
        - name: DESCRIPTION
          value: "create-yum-repo: $(tasks.create-yum-repo.status)"
        - name: SHA
          value: $(params.git-revision)
        - name: PROJECT_ID
          value: $(params.source-project-id)
        - name: STATE
          value: $(tasks.create-yum-repo.status)
        - name: SKIPPED
          value: $(tasks.pipeline-guard.results.skip-image-build)
        - name: NAME
          value: create-yum-repo
        - name: TASKRUN_LOG_NAME
          value: "$(context.pipelineRun.name)_create-yum-repo.log"
      taskSpec: "{{ task_gitlab_set_status }}"

    - name: fusa-quality-gate-set-status
      params:
        - name: DESCRIPTION
          value: "fusa-quality-gate: $(tasks.fusa-quality-gate.status)"
        - name: SHA
          value: $(params.git-revision)
        - name: PROJECT_ID
          value: $(params.source-project-id)
        - name: STATE
          value: $(tasks.fusa-quality-gate.status)
        - name: SKIPPED
          value: $(tasks.pipeline-guard.results.skip-image-build)
        - name: NAME
          value: fusa-quality-gate
        - name: TASKRUN_LOG_NAME
          value: "$(context.pipelineRun.name)_fusa-quality-gate.log"
      taskSpec: "{{ task_gitlab_set_status }}"

    - name: create-yum-repo-upload-logs
      params:
        - name: TASK_NAME
          value: create-yum-repo
        - name: PIPELINE_RUN_NAME
          value: $(context.pipelineRun.name)
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_upload_taskrun_log }}"

    - name: fusa-quality-gate-upload-logs
      params:
        - name: TASK_NAME
          value: fusa-quality-gate
        - name: PIPELINE_RUN_NAME
          value: $(context.pipelineRun.name)
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_upload_taskrun_log }}"

    - name: run-smoke-tests-aarch64-set-status
      params:
        - name: DESCRIPTION
          value: "run-smoke-tests-aarch64: $(tasks.run-smoke-tests-aarch64.status)"
        - name: SHA
          value: $(params.git-revision)
        - name: PROJECT_ID
          value: $(params.source-project-id)
        - name: STATE
          value: $(tasks.run-smoke-tests-aarch64.status)
        - name: SKIPPED
          value: $(tasks.pipeline-guard.results.skip-image-build)
        - name: NAME
          value: run-smoke-tests-aarch64
        - name: TASKRUN_LOG_NAME
          value: "$(context.pipelineRun.name)_run-smoke-tests-aarch64.log"
      taskSpec: "{{ task_gitlab_set_status }}"

    - name: run-smoke-tests-aarch64-upload-logs
      params:
        - name: TASK_NAME
          value: run-smoke-tests-aarch64
        - name: PIPELINE_RUN_NAME
          value: $(context.pipelineRun.name)
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_upload_taskrun_log }}"

    - name: create-osbuild-cs9-aarch64-set-status
      params:
        - name: DESCRIPTION
          value: "create-osbuild-cs9-aarch64: $(tasks.create-osbuild-cs9-aarch64.status)"
        - name: SHA
          value: $(params.git-revision)
        - name: PROJECT_ID
          value: $(params.source-project-id)
        - name: STATE
          value: $(tasks.create-osbuild-cs9-aarch64.status)
        - name: SKIPPED
          value: $(tasks.pipeline-guard.results.skip-image-build)
        - name: NAME
          value: create-osbuild-cs9-aarch64
        - name: TASKRUN_LOG_NAME
          value: "$(context.pipelineRun.name)_create-osbuild-cs9-aarch64.log"
      taskSpec: "{{ task_gitlab_set_status }}"

    - name: create-osbuild-cs9-aarch64-upload-logs
      params:
        - name: TASK_NAME
          value: create-osbuild-cs9-aarch64
        - name: PIPELINE_RUN_NAME
          value: $(context.pipelineRun.name)
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_upload_taskrun_log }}"

    - name: create-osbuild-cs9-x86-64-set-status
      params:
        - name: DESCRIPTION
          value: "create-osbuild-cs9-x86-64: $(tasks.create-osbuild-cs9-x86-64.status)"
        - name: SHA
          value: $(params.git-revision)
        - name: PROJECT_ID
          value: $(params.source-project-id)
        - name: STATE
          value: $(tasks.create-osbuild-cs9-x86-64.status)
        - name: SKIPPED
          value: $(tasks.pipeline-guard.results.skip-image-build)
        - name: NAME
          value: create-osbuild-cs9-x86-64
        - name: TASKRUN_LOG_NAME
          value: "$(context.pipelineRun.name)_create-osbuild-cs9-x86-64.log"
      taskSpec:
        "{{ task_gitlab_set_status }}"

    - name: create-osbuild-cs9-x86-64-upload-logs
      params:
        - name: TASK_NAME
          value: create-osbuild-cs9-x86-64
        - name: PIPELINE_RUN_NAME
          value: $(context.pipelineRun.name)
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec:
        "{{ task_upload_taskrun_log }}"

    - name: verify-pipelinerun-set-status
      params:
        - name: DESCRIPTION
          value: "pipeline status"
        - name: SHA
          value: $(params.git-revision)
        - name: PROJECT_ID
          value: $(params.source-project-id)
        - name: STATE
          value: "success"
        - name: NAME
          value: pipeline-status
      taskSpec: "{{ task_gitlab_set_status }}"
