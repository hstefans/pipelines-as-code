.PHONY: up
.DEFAULT_GOAL := up

# Define where to create the local repo (consumed in create_yum_repo)
export REPO_DIR ?= /var/lib/repos

# start virtual environment
up:
	vagrant up --provider=libvirt
	# ssh in the virtual environment
	vagrant ssh

# destroying virtual environment
destroy:
	vagrant -f destroy

yum-repo-cs9:
	$(MAKE) -C ci yum-repo-cs9
