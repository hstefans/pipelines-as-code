import pytest

import utils


def test_createrepo_succeeded(download_repodir: str) -> bool:
    assert utils.run_cmd(["createrepo_c", download_repodir]) == 0


def test_createrepo_failed() -> bool:
    assert utils.run_cmd(["createrepo_c", "/no/repo/"]) == 1


def test_repo_succeeded(dnf_conf: str, repo: str) -> bool:
    assert utils.run_cmd(["dnf", "-v", "-c", f"{dnf_conf}", "makecache", "--repo=examplerepo"]) == 0


def test_repo_failed(dnf_conf: str, repo: str) -> bool:
    assert utils.run_cmd(["dnf", "-v", "-c", f"{dnf_conf}", "makecache", "--repo=unknownrepo"]) == 1
