import boto3
import os
import pathlib
import pytest
import sys

from botocore.exceptions import ClientError

import main


def test_upload_directory(
    upload_repodir: str, bucket: str, boto3_session: str, s3_client: boto3.client
) -> bool:
    assert main.upload_directory(upload_repodir, "pipeline-run", bucket) == 0
    response = s3_client.list_objects_v2(Bucket=bucket)
    files_in_bucket = []
    for content in response["Contents"]:
        files_in_bucket.append(content["Key"])

    assert "pipeline-run/cs8/test1.rpm" in files_in_bucket
    assert "pipeline-run/cs9/test2.rpm" in files_in_bucket


def test_download_packages_cs8(
    download_repodir: str,
    supported_arches: list,
    repositories: list,
    mirror_list: list,
) -> bool:
    # Repeat download loop three times to ensure directory cleanup works
    package_list_dir = "./tests/resources/manifest_repo/package_list"
    cs = "cs9"
    for i in range(3):
        for supported_arch in supported_arches:
            main.download_packages(
                package_list_dir,
                cs,
                download_repodir,
                supported_arch,
                repositories,
                mirror_list[cs],
            ) == 0

            for repo in repositories:
                assert os.path.exists(f"{download_repodir}/{repo}/{supported_arch}/os") == True

        # Magic number comes from packages in tests/resources/manifest_repo/package_list
        rpms = list((pathlib.Path(download_repodir)).rglob("*.rpm"))
        print(rpms)
        assert len(rpms) == 8


def test_download_packages_cs9(
    download_repodir: str,
    supported_arches: list,
    repositories: list,
    mirror_list: list,
) -> bool:
    # Repeat download loop three times to ensure directory cleanup works
    package_list_dir = "./tests/resources/manifest_repo/package_list"
    cs = "cs9"
    for i in range(3):
        for supported_arch in supported_arches:
            main.download_packages(
                package_list_dir,
                cs,
                download_repodir,
                supported_arch,
                repositories,
                mirror_list[cs],
            ) == 0

            for repo in repositories:
                assert os.path.exists(f"{download_repodir}/{repo}/{supported_arch}/os") == True

        # Magic number comes from packages in tests/resources/manifest_repo/package_list
        rpms = list((pathlib.Path(download_repodir)).rglob("*.rpm"))
        print(rpms)
        assert len(rpms) == 8
