#!/bin/bash
set -eo pipefail

ARCH=$(arch)
UUID=${UUID:-local}
BUILD_TYPE=${BUILD_TYPE:-osbuild}

if [[ "${STREAM}" == "upstream" ]]; then
    OS_PREFIX="cs"
else
    OS_PREFIX="rhel"
fi
OS_VERSION="${OS_VERSION:-9}"
IMAGE_KEY="auto-${BUILD_TYPE}-${OS_PREFIX}${OS_VERSION}-${ARCH}-${UUID}"
DOWNLOAD_DIRECTORY="/var/lib/libvirt/images"
S3_BUCKET_NAME="auto-ci-pipeline-images"
AWS_CLI="aws"
AWS_REGION="eu-west-1"
AWS_TF_REGION="us-east-2"

echo "[+] Install dependencies"
dnf install -y jq python3-pip
pip3 install awscli

echo "[+] Configure AWS settings"
$AWS_CLI configure set default.region "$AWS_REGION"
$AWS_CLI configure set default.output json

if [ ! -r "${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.raw" ]; then
	echo "Error: the file ${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.raw doesn't exist"
	exit 1
fi
echo "[+] Uploading raw image to 's3://${S3_BUCKET_NAME}/${IMAGE_KEY}.raw"
$AWS_CLI s3 cp ${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.raw s3://${S3_BUCKET_NAME} \
	--only-show-errors \
	--acl public-read
$AWS_CLI s3 cp ${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.json s3://${S3_BUCKET_NAME} \
	--only-show-errors \
	--acl public-read

echo "[+] Import snapshot to EC2"
IMPORT_SNAPSHOT_ID=$($AWS_CLI ec2 import-snapshot --disk-container Format=raw,UserBucket="{S3Bucket=${S3_BUCKET_NAME},S3Key=${IMAGE_KEY}.raw}" | jq -r .ImportTaskId)

echo "[+] Waiting for snapshot $IMPORT_SNAPSHOT_ID import"

until [ "$status" == "completed" ]; do
	status=$($AWS_CLI ec2 describe-import-snapshot-tasks --import-task-ids "$IMPORT_SNAPSHOT_ID" | jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.Status)
	echo -n "."
done
echo "snapshot status: $status"

$AWS_CLI ec2 describe-import-snapshot-tasks --import-task-ids "$IMPORT_SNAPSHOT_ID"
SNAPSHOT_ID=$($AWS_CLI ec2 describe-import-snapshot-tasks --import-task-ids "$IMPORT_SNAPSHOT_ID" | jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.SnapshotId)

$AWS_CLI ec2 create-tags --resources "$SNAPSHOT_ID" --tags Key=ServiceComponent,Value=Automotive Key=ServiceOwner,Value=A-TEAM Key=ServicePhase,Value=Prod Key=FedoraGroup,Value=ci

echo "[+] Copy snapshot from ${AWS_REGION} to ${AWS_TF_REGION}"
SNAPSHOT_ID=$($AWS_CLI ec2 copy-snapshot --source-region ${AWS_REGION} --source-snapshot-id "${SNAPSHOT_ID}" --region=${AWS_TF_REGION} | jq -r .SnapshotId)

echo "[+] Waiting for copy to complete"
status=""
until [ "$status" == "completed" ]; do
	status=$($AWS_CLI ec2 describe-snapshots --region "${AWS_TF_REGION}" --snapshot-ids "${SNAPSHOT_ID}" | jq -r .Snapshots[0].State)
	echo -n "."
done
echo "snapshot status: $status"

echo "[+] Register AMI from snapshot"
if [[ "${ARCH}" == "aarch64" ]]; then
	ARCH="arm64"
fi
IMAGE_ID=$(
	$AWS_CLI ec2 register-image --name "${IMAGE_KEY}" \
		--region ${AWS_TF_REGION} \
		--architecture ${ARCH} \
		--virtualization-type hvm \
		--root-device-name "/dev/sda1" \
		--ena-support \
		--boot-mode uefi \
		--block-device-mappings "[
   {
       \"DeviceName\": \"/dev/sda1\",
       \"Ebs\": {
           \"SnapshotId\": \"$SNAPSHOT_ID\"
       }
   }]" | jq -r .ImageId
)

$AWS_CLI ec2 create-tags --resources "$IMAGE_ID" --region=${AWS_TF_REGION} --tags Key=ServiceComponent,Value=Artemis Key=ServiceName,Value=Artemis Key=AppCode,Value=ARR-001 Key=ServiceOwner,Value=TFT Key=ServicePhase,Value=Prod Key=PIPELINE_RUN_ID,Value="${UUID}"

# Wait for image registration
echo "[+] Waiting for image $IMAGE_ID registration"
until [ "$state" == "available" ]; do
	state=$($AWS_CLI ec2 describe-images --image-ids "${IMAGE_ID}" --region ${AWS_TF_REGION} | jq -r .Images[0].State)
	echo -n "."
done

# Give permissions to the Testing Farm provisioner account
echo "[+] Granting permissions for $IMAGE_ID to the Testing Farm provisioner"
aws ec2 modify-image-attribute \
	--image-id ${IMAGE_ID} \
	--region ${AWS_TF_REGION} \
	--launch-permission "Add=[{UserId=125523088429}]"

echo "image state: $state"
$AWS_CLI ec2 describe-images --image-ids $IMAGE_ID --region "${AWS_TF_REGION}"
