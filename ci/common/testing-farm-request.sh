#!/bin/bash

set -e

dnf install -y jq

ENDPOINT="https://api.dev.testing-farm.io/v0.1/requests"

OS_VERSION="${OS_VERSION:-9}"
if [[ "${ARCH}" == "x86_64" ]]; then
    COMPOSE="CentOS-Stream-${OS_VERSION}"
else
    COMPOSE="CentOS-Stream-${OS_VERSION}-aarch64"
fi

if [[ "${STREAM}" == "upstream" ]]; then
    OS_PREFIX="cs"
    DISTRO_NAME="CentOS Stream release 9"
else
    OS_PREFIX="rhel"
    DISTRO_NAME="Red Hat Enterprise Linux release 9.0 Beta (Plow)"
fi

if [[ "${ACTION}" == "TEST" ]]; then
    URL="https://gitlab.com/redhat/edge/tests/base-image"
    REPO_URL="https://gitlab.com/redhat/edge/tests/base-image.git"
    REF="main"
    COMPOSE="auto-osbuild-${OS_PREFIX}${OS_VERSION}-${ARCH}-${UUID}"
else
    URL="https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code"
fi


# Compose the query for calling the Testing Farm API
cat <<EOF > request.json
{
  "api_key": "${TF_API_KEY}",
  "test": {
    "fmf": {
    "url": "${URL}",
    "ref": "${REF}",
    "name": "${TMT_PLAN}"
    }
  },
  "environments": [
   {
   "arch": "${ARCH}",
   "os": {"compose": "${COMPOSE}"},
   "variables": {
     "ARCH": "${ARCH}",
     "UUID": "${UUID}",
     "REPO_URL": "${REPO_URL}",
     "REVISION": "${REVISION}",
     "OS_VERSION": "${OS_VERSION}",
     "IMAGE_NAME": "${IMAGE_NAME}",
     "DISTRO_NAME": "${DISTRO_NAME}",
     "STREAM": "${STREAM}",
     "TF_SSH_KEY": "${TF_SSH_KEY}"
     },
   "secrets": {
     "AWS_ACCESS_KEY_ID": "${AWS_ACCESS_KEY_ID}",
     "AWS_SECRET_ACCESS_KEY": "${AWS_SECRET_ACCESS_KEY}",
     "REGION": "${AWS_REGION}",
     "BUCKET": "${AWS_BUCKET_REPOS}"
     }
   }
   ]
}
EOF

# Do the API query to Testing Farm with the data from above
curl --silent ${ENDPOINT} \
    --header "Content-Type: application/json" \
    --data @request.json \
    --output response.json

# Show the response, but hide the secrets values
jq 'del(.environments |. [] | .secrets)' response.json

ID=$(jq -r '.id' response.json)
echo "Wait until the job finished at the Testing Farm"
while true; do
    rm -f response.json
    curl --silent --output response.json "${ENDPOINT}/${ID}"
    STATUS=$(jq -r '.state' response.json)
    if [[ "$STATUS" == "complete" ]] || [[ "$STATUS" == "error" ]]; then
        echo ; echo "Finished"
        break
    fi
    echo -n "."
    sleep 30
done

# Check the tests result
RESULT=$(jq -r '.result.overall' response.json)
echo "Result: $RESULT"

EXIT_CODE=1
if [[ "$RESULT" == "passed" ]]; then
    EXIT_CODE=0
fi

# Even in failure, there are artifacts and a pipeline.log
ARITFACTS_URL=$(jq -r '.run.artifacts //""' response.json)

# If the result is an error, there is no report to show and means that something
# was wrong with the call or there was an internal error from Testing Farm.
# It does not mean that the tests failed.
if [[ "$RESULT" == "error" ]]; then
    EXIT_CODE=10
    echo "Testing Farm has returned an error:"
    jq -r '.result.summary' response.json
fi

# Show the artifacts URL in the log
if [[ -n "$ARITFACTS_URL" ]]; then
    echo "Testing Farm artifacts: $ARITFACTS_URL"
    echo "Testing Farm pipeline log: $ARITFACTS_URL/pipeline.log"
    echo -n "$ARITFACTS_URL" > ARITFACTS_URL
fi

exit $EXIT_CODE
