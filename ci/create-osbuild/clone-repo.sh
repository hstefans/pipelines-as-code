#!/bin/bash

set -euxo pipefail

MAIN_REPO="https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
REPO_URL=${REPO_URL:-$MAIN_REPO}
OSBUILD_DIR=${OSBUILD_DIR:-osbuild-manifests}

if [[ "${REPO_URL}" == "${MAIN_REPO}" ]]; then
    REPO_URL="https://gitlab.com/redhat/automotive/automotive-sig.git"
    REVISION="main"
fi

if ! [[ -v REVISION ]]; then
    echo "Error: The ENV variable REVISION is missing"
    exit 1
fi

echo "[+] Install Git"
sudo dnf install -y git

echo "[+] Remove any previous manifests"
rm -vfr "${OSBUILD_DIR}"
echo "[+] Clone the repository with the manifests"
git clone "${REPO_URL}" "${OSBUILD_DIR}"
cd "${OSBUILD_DIR}"
git reset --hard "${REVISION}"

echo "[+] Cloned"
